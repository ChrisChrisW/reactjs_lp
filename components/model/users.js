export let Users = [
  {
    id: 1,
    email: "test.user@react.dev",
    username: "test",
    password: "password",
    userToken: "testtoken",
  },
  {
    id: 2,
    email: "christophe@react.dev",
    username: "cwang",
    password: "password",
    userToken: "christophetoken",
  },
  {
    id: 3,
    email: "antony.constantin@oktopod.dev",
    username: "aconstantin",
    password: "password",
    userToken: "constantintoken",
  },
];
